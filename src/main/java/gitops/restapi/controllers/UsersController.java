package gitops.restapi.controllers;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import gitops.restapi.models.db.User;
import gitops.restapi.services.UserService;

@RestController
public class UsersController {

    private final UserService service;

    @Autowired
    public UsersController(
        UserService userService
    ) {
        this.service = userService;
    }

    // curl -v localhost:8080/api/users
    @GetMapping("/api/users")
    public ResponseEntity<List<User>> list() {
        return ResponseEntity.ok(service.list());
    }

    // curl -v localhost:8080/api/users/1
    @GetMapping("/api/users/{id}")
    public ResponseEntity<User> show(@PathVariable Long id) {
        return okOrNotFound(() -> service.findById(id));
    }

    // curl -v -X POST -H "Content-Type: application/json" -d '{"firstName":"Max","lastName":"Müller","email":"max@example.org","age":42}' localhost:8080/api/users
    @PostMapping("/api/users")
    public ResponseEntity<User> create(@RequestBody User user) {
        User persisted = service.create(user);
        return ResponseEntity.created(
            UriComponentsBuilder.fromPath("/api/users/{id}")
            .buildAndExpand(persisted.getId()).toUri()
        ).body(persisted);
    }

    // curl -v -X PUT -H "Content-Type: application/json" -d '{"firstName":"Max","lastName":"Schmidt","email":"max@example.org","age":42}' localhost:8080/api/users/3
    @PutMapping("/api/users/{id}")
    public ResponseEntity<User> update(@PathVariable Long id, @RequestBody User user) {
        return okOrNotFound(() -> service.update(id, user));
    }

    // curl -v -X DELETE localhost:8080/api/users/3
    @DeleteMapping("/api/users/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

    // curl -v localhost:8080/api/users/oldest
    @GetMapping("/api/users/oldest")
    public ResponseEntity<User> getOldest() {
        return okOrNotFound(() -> service.findOldest());
    }

    // curl -v localhost:8080/api/users/youngest
    @GetMapping("/api/users/youngest")
    public ResponseEntity<User> getYoungest() {
        return okOrNotFound(() -> service.findYoungest());
    }

    private <T> ResponseEntity<T> okOrNotFound(Supplier<Optional<T>> supplier) {
        return supplier.get()
        .map(ResponseEntity::ok)
        .orElseGet(() -> ResponseEntity.notFound().build());
    }
    
}
